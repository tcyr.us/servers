const Fiber = require('fibers');

module.exports = {
  publicPath: './',
  css: {
    loaderOptions: {
      sass: {
        implementation: require('sass'),
        sassOptions: {
          fiber: Fiber,
        },
      }
    }
  }
}
