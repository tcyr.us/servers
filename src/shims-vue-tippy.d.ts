declare module 'vue-tippy' {
  import Vue, { PluginObject, PluginFunction, ComponentOptions } from 'vue';

  export default class VueTippy implements PluginObject<{}> {
    [key: string]: any;
    public static install(pVue: typeof Vue, options?: {} | undefined): void;
    public install: PluginFunction<{}>;
  }

  export class TippyComponent implements ComponentOptions<Vue> {
  }
}
