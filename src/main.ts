import Vue from 'vue';
import VueTippy, { TippyComponent } from 'vue-tippy';
import App from './App.vue';
import store from './store';

Vue.config.productionTip = false;

Vue.use(VueTippy);

Vue.component('tippy', TippyComponent);

new Vue({
  store,
  render: (h) => h(App),
}).$mount('#app');
