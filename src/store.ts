import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';

Vue.use(Vuex);

const dataUrl = 'https://gist.githubusercontent.com/tcyrus/e6b940b05e96cd35136a35872e7a8e2e/raw/servers.json';

export default new Vuex.Store({
  state: {
    servers: [],
  },

  actions: {
    loadServers({ commit }) {
      axios.get(dataUrl).then((response) => {
        commit('setServers', response.data);
      });
    },
  },

  mutations: {
    setServers(state, servers) {
      state.servers = servers;
    },
  },
});
