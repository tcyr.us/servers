export interface ServerStoreState {
  name: string;
  icon: string;
  status: string;
  platform: string;
  os: string;
  loc: string;
  desc: string;
}
